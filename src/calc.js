module.exports.sum = function sum(a,b, ...rest) {
    return a + b + rest.reduce((acc,val) => acc + val, 0);
}
